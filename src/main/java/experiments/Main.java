package experiments;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import experiments.stagDataModel.RozvrhUcebny;
import experiments.graphDataModel.*;
import experiments.stagDataModel.RozvrhovaAkce;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        Properties config = new Properties();
        config.load(new FileInputStream("app.config"));

        int rokOd = Integer.parseInt(config.getProperty("rokOd"));
        int rokDo = Integer.parseInt(config.getProperty("rokDo"));
        String mistnost = config.getProperty("mistnost");
        String semestr = config.getProperty("semestr");

        ArrayList<Integer> values = new ArrayList<>();

        for(int rok = rokOd; rok <= rokDo; rok++)
        {
            String url = "https://stag-demo.uhk.cz/ws/services/rest2/rozvrhy/" +
                    "getRozvrhByMistnost" +
                    "?semestr=" + semestr +
                    "&rok=" + rok +
                    "&budova=J" +
                    "&mistnost=" + mistnost +
                    "&outputFormat=json";
            URL url1 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            java.util.Scanner responseScanner = new java.util.Scanner(conn.getInputStream()).useDelimiter("\\A");
            String responseString = responseScanner.next();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            RozvrhUcebny rozvrhUcebny = gson.fromJson(responseString, RozvrhUcebny.class);

            int counter = 0;
            for(RozvrhovaAkce akce : rozvrhUcebny.getRozvrhovaAkce())
            {
                counter += akce.getObsazeni();
            }
            values.add(counter);
        }

        ArrayList<Object> series = new ArrayList<Object>();
        series.add("Počet studentů");
        for (int value : values)
        {
            series.add(value);
        }

        Graph graph = new Graph();
        graph.bindto ="#chart";
        graph.data = new Data(new Object[][]{series.toArray()});

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String json = gson.toJson(graph);
        Path path = Paths.get(System.getProperty("user.dir"),"graph2.json");
        FileWriter fw = new FileWriter(path.toString());

        fw.write(json);
        fw.close();
    }
}
